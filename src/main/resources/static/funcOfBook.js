function selectAll() {
    if (document.getElementById("selectedAll").checked == true) {
        var checkboxes = document.getElementsByName('checkUnit');
        for (var i = 0; i < checkboxes.length; i++) {
            checkboxes[i].checked = true;
        }
    } else {
        var checkboxes = document.getElementsByName('checkUnit');
        for (var i = 0; i < checkboxes.length; i++) {
            checkboxes[i].checked = false;
        }
    }
}

function check() {
    var mark = true;
    var checkboxes = document.getElementsByName('checkUnit');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked == false) {
            mark = false;
        }
    }
    if (mark == true) {
        document.getElementById("selectedAll").checked = true;
    } else {
        document.getElementById("selectedAll").checked = false;
    }
}

function updateUnit() {
    var checkboxes = document.getElementsByName('checkUnit');
    var cnt = 0;
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked == true) {
            cnt++;
        }
    }
    if (cnt == 0) {
        alert("Hãy chọn Book để thực hiện chức năng Update!");
    } else if (cnt >= 2) {
        alert("Không thể thực hiện Update cho " + cnt + " Book cùng một lần! Mời chọn lại!");
    } else {
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].checked == true) {
                var checkValue = checkboxes[i].value;
                window.location = 'http://localhost:8080/updateBook?id=' + checkValue;
                break;
            }
        }
    }
}

function deleteBook() {
    var checkboxes = document.getElementsByName('checkUnit');
    var cnt = 0;
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked == true) {
            cnt++;
        }
    }
    if (cnt == 0) {
        alert("Hãy chọn Book để thực hiện Delete!");
    } else {
        if (confirm("Bạn muốn xoá những Book đã chọn không?")) {
            var id = "";
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].checked == true) {
                    var checkValue = checkboxes[i].value;
                    if (id != "") {
                        id = id + "," + checkValue;
                    } else {
                        id = checkValue;
                    }
                }
            }
            window.location = 'http://localhost:8080/deleteBook?id=' + id;
        }
    }
}
