package techja.webdev.bookmanagement.models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "book")
public class Book {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id ;

	@Column(name = "name")
	private String name ;

	@Column(name = "author")
	private String author;

	@Column(name = "price")
	private String price;

	@Column(name = "publishing_year")
	private String publishingYear;
}
