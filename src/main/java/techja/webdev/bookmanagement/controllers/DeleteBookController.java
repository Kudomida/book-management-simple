package techja.webdev.bookmanagement.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import techja.webdev.bookmanagement.services.BookService;

@Controller
public class DeleteBookController {

    @Autowired
    BookService bookService;

    @GetMapping(value = "/deleteBook")
    public String update(@RequestParam(value = "id", required = false) String id) {
        bookService.deleteBook(id);
        return "redirect:/viewBookList";
    }
}
