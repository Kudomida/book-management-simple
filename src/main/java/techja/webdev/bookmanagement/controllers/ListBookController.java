package techja.webdev.bookmanagement.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import techja.webdev.bookmanagement.models.Book;
import techja.webdev.bookmanagement.repositories.BookRepository;
import techja.webdev.bookmanagement.services.BookService;

@Controller
public class ListBookController {

	@Autowired
	BookService bookService;

	@RequestMapping(value = "viewBookList", method = RequestMethod.GET)
	public String view(Model model){
		List<Book> bookList = bookService.getBookList();
		String msg = bookService.getMessage();
		model.addAttribute("msg", msg);
		model.addAttribute("bookList", bookList);
		return "List";
	}

}
