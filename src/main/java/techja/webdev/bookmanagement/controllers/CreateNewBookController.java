package techja.webdev.bookmanagement.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import techja.webdev.bookmanagement.services.BookService;

@Controller
public class CreateNewBookController {

	@Autowired
	BookService bookService;

	@GetMapping(value = "createNewBook")
	public String createNewBook(){
		return "Create";
	}

	@PostMapping(value = "createNewBook")
	public String saveNewBook(@RequestParam(value = "name") String name,
			@RequestParam(value = "author") String author,
			@RequestParam(value = "price") String price,
			@RequestParam(value = "publishingYear") String publishingYear
			){
		bookService.createNewBook(name, author, price, publishingYear);
		return "redirect:/viewBookList";
	}
}
