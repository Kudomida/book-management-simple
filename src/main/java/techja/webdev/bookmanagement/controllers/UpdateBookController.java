package techja.webdev.bookmanagement.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import techja.webdev.bookmanagement.models.Book;
import techja.webdev.bookmanagement.services.BookService;

@Controller
public class UpdateBookController {

	@Autowired
	BookService bookService;
	
	@GetMapping(value = "/updateBook")
	public String update(@RequestParam(value = "id", required = false) String id, final Model model) {
		Book book = bookService.findBookById(id);
		model.addAttribute("bookInf", book);
		return "Update";
	}
	
	@PostMapping(value = "/updateBook")
	public String saveBook(@RequestParam(value = "id") String id,
						   @RequestParam(value = "name") String name,
						   @RequestParam(value = "author") String author,
						   @RequestParam(value = "price") String price,
						   @RequestParam(value = "publishingYear") String publishingYear) {
		bookService.updateBook(id, name, author, price, publishingYear);
		return "redirect:/viewBookList";
	}
}
