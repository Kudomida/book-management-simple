package techja.webdev.bookmanagement.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import techja.webdev.bookmanagement.models.Book;
import techja.webdev.bookmanagement.repositories.BookRepository;

import java.util.List;

@Service
public class BookService {

	@Autowired
	BookRepository bookRepository;

	public List<Book> getBookList(){
		return bookRepository.findAll();
	}

	public String getMessage(){
		String msg = "";
		if (getBookList().size() == 0){
			msg = "Không có Book để hiển thị!";
		}
		return msg;
	}

	public void createNewBook(String name, String author, String price, String publishingYear ){
		Book book = new Book();
		book.setName(name);
		book.setAuthor(author);
		book.setPrice(price);
		book.setPublishingYear(publishingYear);
		bookRepository.save(book);
		System.out.println("Create new book success!");
	}

	public Book findBookById(String id){
		return bookRepository.findBookById(Integer.parseInt(id));
	}

	public void updateBook(String id, String name, String author, String price, String publishingYear){
		Book book = bookRepository.findBookById(Integer.parseInt(id));
		book.setName(name);
		book.setAuthor(author);
		book.setPrice(price);
		book.setPublishingYear(publishingYear);
		bookRepository.save(book);
		System.out.println("Update book success!");
	}

	public void deleteBook(String id){
		String[] idArr =id.split(",");
		for (int i = 0; i <idArr.length ; i++) {
			bookRepository.delete(bookRepository.findBookById(Integer.parseInt(idArr[i])));
		}
		System.out.println("Delete book success!");
	}

}
