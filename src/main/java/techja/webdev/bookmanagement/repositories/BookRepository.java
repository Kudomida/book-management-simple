package techja.webdev.bookmanagement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import techja.webdev.bookmanagement.models.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

	/**
	 * Find Book by id of book
	 * @param id	id of book
	 * @return	Book Entity
	 */
	Book findBookById(Integer id);
}
